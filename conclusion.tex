\begin{conclusion}
\label{chapter:conclusion}

Single molecule localization microscopy reconstructs a super-resolution image from positions of individual molecules located in time series data recorded by a camera. This allows further analysis of the spatial distribution of localized molecules, e.g., cluster analysis, which helps to understand the nano-scale distribution of proteins inside cells.

\section{Summary}

In this thesis we provide a comprehensive study of processing the single molecule localization microscopy data. We start with principles of image formation, discuss the problem of the detection and localization of molecules, and finish with rendering super-resolution images.

We have developed a set of experiments in MATLAB language which helped us to analyze properties of individual algorithms used in different steps of the data processing:

\begin{itemize}
	\item \textbf{Detection of the molecules (\refsec{experiment_detection}).} A Monte-Carlo simulation was designed to evaluate detection rate of different combinations of noise filters and detectors with respect to the signal-to-noise ratio. Our results suggest $3 \times 3$ box filter followed by the morphological filter with radius of 7 pixels as a best detector.

	\item \textbf{Localization of the molecules (\refsec{experiment_2dloc}).} Our experiments confirmed that in case of the data with Poisson distributed noise, maximum-likelihood estimation of a molecule position is more accurate than applying least-squares methods. However, for low signal-to-noise ratio, the least-squares methods outperform the maximum-likelihood estimator. We also compared accuracy of position estimation by fitting different PSF models. Overall, the best approximation was achieved by Airy disk. Nonetheless, a Gaussian PSF model provides sufficient approximation, especially with least-squares methods where the difference in precision is negligible. We also observed that a parabola PSF model is suitable for precise localization of molecules with low signal-to-noise ratio, even though the parabola does not approximate the real PSF well, which limits the application of this model only for a localization in two dimensions.

	\item \textbf{Crowded-field problem (\refsec{experiment_crowded_field}).} It was demonstrated that the detection rate of common localization methods decreases with increasing spatial density of the molecules. To address this problem, we used two specialized algorithms called CSSTORM and DAOSTORM. Our experiments has shown that the best detection rate is obtained by CSSTORM. However, this method is very slow and it also produces many false-positive detections. In contrast, DAOSTORM is a relatively fast method with slightly worse detection rate than CSSTORM, but with much less false-positive detections.

	\item \textbf{2D data visualization (\refsec{experiment_2d_visualization}).}  We proposed to divide rendering methods into two distinct groups: scatter-based methods and area-based methods. In order to evaluate quality of the rendering methods, we designed a measure based on the Separation Index. Consequently, we compared performance of the rendering algorithms in a line separation tests and in an intersection distinguishability tests. Our results demonstrated that the scatter-based methods are better for rendering complex structures, such as mesh or fibers. On the other hand, area-based method are better for rendering uniform areas or surfaces.

	\item \textbf{3D data visualization (\refsec{developing_3d_rendering_methods}).} Since a significant part of our effort concentrates on extraction of three-dimensional coordinates of the molecules from the input data, we desire to visualize the molecules in three-dimensional space. Currently used methods for 3D rendering are maximal intensity projection and 3D histogram. However, with these methods, we often lose information about depth or density of the molecules. To address this problem, we have developed a method for 3D rendering, which solves these issues by techniques known from computer graphics, e.g., geometric transformations, $z$-culling, or alpha-blending.
	
	\item \textbf{3D calibration and real data analysis (Sections \ref{sec:experiment_calibration3D} and \ref{sec:real_data_analysis}).} In order to localize the molecules in three dimensions, we used the astigmatic imaging. The microscope system was first calibrated with fluorescent beads. Then we applied the calibrated method to the real dataset, i.e., ErbB3 molecules in the membrane of A431 cells. Finally, we rendered the super-resolution image of the cell as shown in \reffig{real_data_analysis}.

\end{itemize}


\section{Contributions}

Main contributions of this thesis are the following:

\begin{itemize}
	\item \textbf{Detection of molecules.} In \refsec{experiment_detection} we demonstrated that a box filter followed by a morphological detector achieves even better results than a commonly used convolution with a Gaussian kernel followed by a maxima detection.

	\item \textbf{Localization of molecules.} We showed that a parabola can be successfully used as a PSF model for estimating the positions of molecules. Moreover, in \refsec{experiment_2dloc} we observed that the non-linear least-squares fitting of the parabola PSF model can achieve a relatively small localization error, especially in areas with low signal to noise ratio, which is due to the convexity of parabola.

	\item \textbf{2D data visualization.} In order to computationally evaluate quality of different 2D rendering methods, we proposed a measure based on the Separation Index (\refsec{experiment_SI}). Also, we designed an experiment evaluating plausibility of rendering intersections of two lines since this is a usual situation taking place in cells (\refsec{experiment_2d_crossing}). We also proposed a taxonomy to classify the rendering methods according to their properties, which simplifies a process of selecting a suitable rendering method for particular data.

	\item \textbf{3D data visualization.} In \refsec{developing_3d_rendering_methods} we developed a methodology for rendering super-resolution images and movies in 3D datasets using 2D visualization methods.

\end{itemize}


\section{Future work}

One of the topics described in the thesis is a method for 3D localization of the molecules called Dual objective multifocal plane microscopy (\refsec{3d_biplane}). However, we did not have the appropriate microscope configuration available at the time of writing this thesis. One of the main goals for future research is to analyse data captured by this microscope and to compare the results with the results obtained by astigmatic imaging.

Another possible area for future research is the crowded-field detection and localization problem, which still presents a challenge in a matter of detection rate and computational complexity. Furthermore, it is desirable to develop a fast crowded-field solver with ability of 3D localization.

\end{conclusion}