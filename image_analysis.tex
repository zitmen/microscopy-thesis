\chapter{Image analysis}
\label{chapter:image_analysis}

There are many methods to detect and localize individual molecules. The methods can be combined with each other, however, two typical processes were established. We call them direct and indirect localization, see \reffig{direct+indirect_fit}. The input for both of the processes is a~sequence of images captured by a~camera attached to the microscope. The output is a~list of positions of the molecules.

\begin{figure}[h!]
    \centering
    \includegraphics[width=15cm]{images/direct+indirect_fit}
    \caption[Localization process diagram.]{Localization processes: (a)~direct and (b)~indirect localization. The rectangles represent action, the cylinders mean direct data access, and the curved rectangles denote stored data.}
    \label{fig:direct+indirect_fit}
\end{figure}

Direct localization takes a~raw input image and runs the PSF model fitting routine on it, resulting in a~list of molecules, which is the output of the whole process. This procedure can be described as a~brute-force procedure, because we have no list of areas where we should seek for the individual molecules hence the fitting is done for each pixel in the image. Parameters of the estimated models are further analysed and a~thresholding is applied, i.e., it is determined whether the peak amplitude of a~PSF model is sufficient and if its width is in a~certain range.

In contrast, the indirect localization is a~process that has several stages. First, noise is reduced and non-uniform background is suppressed in the input image, then the list of candidates is obtained from these modified images. In the next step, initial positions of PSF models are set to the positions of candidates and later fitted in the input raw image. Outputs are more precise positions (sub-pixel localizations), peak intensities, and widths of the PSF models. The thresholding is performed as well as in the direct method, giving us the resulting list of molecules.

Thus, the difference between the two processes is that the indirect localization uses pre-processing to obtain positions where the molecules could be found while the direct fitting tries all positions possible. Both processes might use a~post-processing which seeks to minimize false-positive errors.

\emph{Note:} An~image is generally a~discrete grid of pixels. Despite this, where possible, we present equations in continuous form, because it is better for scaling the resolution. The discretization process is straightforward.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Noise filtering}
Since quality of an~input image is degraded by noise, which would adversely affect the detection error, it is necessary to process the image by a~noise filter. Selection of a~good filter can have a~positive impact on the quality of the whole localization process \cite{ORIGINAL}.

\subsection{Median filter}
A~median filter is a~non-linear image filter in which each pixel in the resulting image has a~value equal to the median of its neighboring pixels. With a~two-dimensional median filter, which is our case, it is possible to use either "box" or "cross" pattern to determine which neighbors will be included to the median calculation. If the pattern has an~odd number of entries, the median is the middle of all numerically sorted entries. For an~even number of entries the median is usually calculated as the mean of the two middle entries.

For small to moderate levels of (Gaussian) noise, for speckle noise, and for salt and pepper noise, the median filter is demonstrably better than Gaussian blur at removing noise whilst preserving edges \cite{SIGNAL_PROCESSING_BOOK,MEDIAN_VS_GAUSS}. However, its performance is not that much better than Gaussian blur for high level of Gaussian noise.

\subsection{Box filter}
\label{sec:box_filter}
Box filter (also called Average Filter) is an~image filter in which each pixel of the resulting image has a~value equal to the average value of its neighboring pixels. Box filter is a~form of low-pass (blurring) filter and is a~convolution (with a~square convolution kernel filled by ones). This convolution brings the value of each pixel into closer harmony with the values of its neighbors.

\subsection{Butterworth filter}
This low-pass filter, as well as other low-pass filters, exploits the fact that there are no major intensity changes in the original signal, i.e., the signal has a~relatively low frequency. In contrast, noise is typically a~high frequency signal. The objective therefore is to cut parasitic high frequencies and preserve the low frequencies to maintain the original signal.

This can be achieved by converting the input signal into the frequency domain using the Fourier Transform, where the high frequency signal is suppressed and then it is converted back into the spatial domain using the Inverse Fourier Transform.

The Fourier Transform in two dimensions is defined by
\begin{equation}
	F(u,v) = \int_{-\infty}^{+\infty}{\int_{-\infty}^{+\infty}{f(x,y) \exp{-2\pi \mathrm{i}(ux+vy)}\mathrm{d}x}\mathrm{d}y} \; ,
	\label{eq:fourier}
\end{equation}
where $\mathrm{i}$ is the imaginary unit of a~complex number, $f(x,y)$ is the input function in the spatial domain, the $F(u,v)$ is the output function in the frequency domain, the pair ${x,y}$ specifies a~position, and the pair ${u,v}$ specifies a~frequency and orientation. Since the output is represented by complex numbers and we need to work with amplitudes of the frequencies, we have to define the signal amplitude in frequency domain as
\begin{equation}
	|F(u,v)| = \sqrt{\mathrm{Re}(F(u,v))^2 + \mathrm{Im}(F(u,v))^2} \; ,
	\label{eq:amplitude_frequencies}
\end{equation}
where Re extracts the real part of a~complex number and analogously Im extracts the imaginary part.

The Inverse Fourier Transform is defined by
\begin{equation}
	f(x,y) = \int_{-\infty}^{+\infty}{\int_{-\infty}^{+\infty}{F(u,v) \exp{+2\pi \mathrm{i}(ux+vy)}\mathrm{d}u}\mathrm{d}v}\; .
	\label{eq:inverse_fourier}
\end{equation}

In \reffig{butterworth_filter} you can see how the filtering part is done. Cutting off the high frequency components is achieved by multiplying the signal amplitudes from \refeq{amplitude_frequencies} in frequency domain by
\begin{equation}
	BLPF(u,v) = \frac{1}{1+\left(\frac{\sqrt{u^2+v^2}}{\sigma}\right)^{2\gamma}} \; .
\end{equation}
In one part of the equation ($\frac{\sqrt{u^2+v^2}}{\sigma}$), we can see an~analytical equation of a~circle, so it is obvious that the parameter $\sigma$ determines the radius of the circle. The larger the $\sigma$, the larger the circle, and thereby the range of the preserved frequencies. The parameter $\gamma$ determines how steep the transition on the boundary of the circle is. The larger $\gamma$, the steeper transition.

\begin{figure}[h!]
    \centering
    \includegraphics[width=13cm]{images/butterworth}
    \caption[Butterworth low-pass filtering process.]{Butterworth low-pass filtering process. The input image in frequency domain is multiplied by BLPF ($\sigma=20,\gamma=5$) and then the Inverse Fourier Transform is applied on the result of multiplication to retrieve the filtered image. Abbreviation (I)DFT stands for the (Inverse) Discrete Fourier Transform, which is just a~discretized (Inverse) Fourier Transform defined by Equations \ref{eq:fourier} and \ref{eq:inverse_fourier}.}
    \label{fig:butterworth_filter}
\end{figure}

\subsection{Convolution filters}
Convolution filters are a~great way to process images for certain features. Features are defined by a~convolution kernel. A~two dimensional convolution,
\begin{equation}
	(F*G)(s,t) = \int_{-\infty}^{+\infty}{\int_{-\infty}^{+\infty}{F(x,y) G(s-x,t-y) \; \mathrm{d}x\mathrm{d}y}}
\end{equation}
can be interpreted as a~sliding average of function $F$ weighted by function $G$ (the convolution kernel).

\subsubsection{Gaussian kernel}
A~Gaussian kernel (in our case a~2D kernel) is simply a~matrix $m$ by $n$ that contains values of the rotationally symmetric Gaussian function such that its integral is equal to one:
\begin{equation}
	G(x,y) = \frac{1}{2 \pi \sigma^2} \exp{\left(- \frac{(x-x_0)^2 + (y-y_0)^2}{2 \sigma^2}\right)}
	\label{eq:normalized_symmetric_gauss_2d_I1}
\end{equation}
Convolution with a~Gaussian kernel is called Gaussian blur. It is a~low-pass filter, attenuating signals with high spatial frequencies.
The advantage of the convolution with this kernel is its tendency to accentuate blobs with the same $\sigma$ as was used in $G(x,y)$ while suppressing others.

\subsubsection{Lowered Truncated Gaussian kernel}
\label{sec:lowered_truncated_gaussian}
This kernel is just a~modified version of the previously mentioned Gaussian kernel which has been lowered to have its integral equal to zero:
\begin{equation}
	LG(x,y) = G(x,y) - E(G(x,y)) \; ,
\end{equation}
where $E$ stands for mean value. Subsequently, part of the Gaussian below zero is truncated:
\begin{equation}
	LG(x,y) = \left\{
	\begin{array}{l l}
		0 & \quad \text{if $LG(x,y) > 0$}\\
		LG(x,y) & \quad \text{if $LG(x,y) \leq 0$}\\
	\end{array} \right.
	\label{eq:lg_trunc}
\end{equation}

\begin{figure}[h!]
    \centering
    \includegraphics[width=14cm]{images/lowered_gauss}
    \caption[Lowered (truncated) Gaussian.]{(a)~Lowered Gaussian and the cut-off plane located at $z=0$. (b)~Lowered truncated Gaussian.}
    \label{fig:lowered_gauss}
\end{figure}

This kernel has been introduced in the DAOPHOT project \cite{DAOPHOT} dealing with astronomical data. It was found that the convolution with the lowered truncated Gaussian kernel has some good features, such as the ability to suppress the background, filter out bad pixels, suppress over-saturated point-like signals, etc.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Flat-field correction}
\label{sec:flat_field_correction}
One of the sources of the image quality degradation is flat-field which is a~non-uniform illumination and also a~non-uniform background caused by out-of-focus molecules emitting photons. Many ways to suppress the background can be found in the literature, however, the majority of those methods are not suitable for our data. So, again we go for inspiration to the astronomers and their DAOPHOT project \cite{DAOPHOT} but in this case we had to make some changes to the algorithm to be effective for our purposes.

First step of the method is to manually select a~subset of the input image sequence, such that the frames are not to bright. Generally it is enough to discard the beginning of the sequence when the shutter opens. Then the algorithm calculates median of all intensities for each individual pixel throughout the whole sequence. The result, image of medians of pixels at each position, is an~estimate of the background. Then if we want to correct an~image for the background, we just subtract the background estimate from the input image. The algorithm can be expressed by the following two equations. The flat-field corrected image $\hat{I}$ with subtracted background can be computed as
\begin{equation}
	\widehat{BG}(x,y) = \mathrm{median}(\{I_1(x,y), ..., I_N(x,y)\}) \; ,
\end{equation}
where $\widehat{BG}$ is a~background estimation, $I_i$ represents $i$th image of the input sequence, and the pair $x,y$ is a~pixel position.
\begin{equation}
	\hat{I}(x,y) = I(x,y) - \widehat{BG}(x,y) + \epsilon \; ,
\end{equation}
where $\epsilon$ is an~offset ensuring all pixels in the flat-field corrected image have positive intensities.

\begin{figure}[h!]
    \centering
    \includegraphics[width=13cm]{images/flatfield_correction}
    \caption[Result of flat-field estimation.]{Result of flat-field estimation: (a)~a~random image from the input sequence and (b)~an~estimated non-uniform background. The cell shape can be seen in the background image. \emph{Note: for illustration purposes, both images were adjusted using a~series of non-linear filters to achieve a~better contrast.}}
    \label{fig:flatfield_correction}
\end{figure}

The algorithm is based on the assumption that individual molecules are mostly dark and they blink only for a~few frames, whilst the background is present the whole time with none or only a~slight change of sensitivity. Therefore, the choice of median is obvious. We could also successfully use mode or mean, but these are too sensitive for shorter image sequences because the mean gets significantly higher with a~blink of a~molecule, thus the subtraction of the estimated background would suppress valid molecules. The mode is problematic if there is no dominant intensity. Because of the random noise, it is very likely to happen, that the result might be also a~random noise. This could be solved by making a~histogram of the intensities and calculating the mode as the most occupied bin but it is actually just an~approximation of median.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Localization of molecules}
\label{sec:molecules_localization}
As specified in the Introduction, the main goal is to localize as many molecules as possible in input images with a~low error. Here we describe mathematical and algorithmic tools needed to succeed at this task. Our results are presented later in \refchapter{experiments}.

\subsection{Maxima detection}
There are two different approaches for maxima detection that are often used. First one uses a~direct comparison with its neighbors (4-cross or 8-box patterns) and the other one uses methods of mathematical morphology.

\subsubsection{Neighborhood method}
This method passes through each pixel in an~input image and checks if the center point is greater or equal compared to its 4 or 8 neighborhood, see \reffig{maxima_detection}. Because the positions of these maxima are determined with an~accuracy of one pixel, this method is usually used for searching for candidates, not for the final localization where we require sub-pixel accuracy.

\begin{figure}[h!]
    \centering
    \includegraphics[width=6cm]{images/maxima_detection}
    \caption[Neighborhood maxima detector example.]{(a)~The center pixel is considered to be a~maximum, because its intensity is greater or equal than its 4-neighborhood but in (b)~the pixel is not a~maximum compared to its 8-neighborhood.}
    \label{fig:maxima_detection}
\end{figure}

\subsubsection{Non-maximal suppression}
Another method for localization of the molecules with one pixel accuracy is called non-maximal suppression. This method just sets all non-maximal pixels to zero. There are several ways to achieve this. We used a~procedure known from mathematical morphology - dilation. Dilation for a~gray-scale image is defined as follows
\begin{equation}
	(f \oplus b)(x) = \sup_{y \in E}[f(y) + b(x-y)] \; ,
\end{equation}
where $f(x)$ is the image, $b(x)$ is a~dilation structuring function, $\sup$ stands for supremum, and $E$ is a~set in which we wish to find the supremum. In our case $E$ is a~discrete grid of pixels.

Local maxima are extracted by performing a~gray-scale morphological dilation and then finding points in the original image that are greater than a~threshold and match the dilated image. It is also possible to specify a~radius of the dilation, so we can easily tweak behavior of the algorithm.

\subsection{Gradient-field method}
Another method for sub-pixel localization was introduced in \cite{GRADIENTS}. This method is much faster than the surface fitting methods described in \refsec{surffit} and at the same time achieves similar accuracy.

This approach uses the fact that for a~radially symmetric intensity distribution (a~molecule) we can draw a~line parallel to the gradient through an~arbitrary point and the line intersects the center of the molecule. Ideally, distance between the line and the center is equal to zero. This will not be exact in a~noisy pixelated image, however, the center can be estimated by minimizing a~sum of distances between the point itself and all such lines.

\begin{figure}[h!]
    \centering
    \includegraphics[width=14cm]{images/gradient_field}
    \caption[Gradient-field estimator principle.]{(a)~shows a~smooth Gaussian with its center marked by a~red X, (b)~shows a~pixelated Gaussian that is processed by converting to the (c)~gradient domain. The yellow vectors (gradients between the pixels) point to the center of the Gaussian.}
    \label{fig:gradient_field}
\end{figure}

First, we have to calculate the gradients
\begin{equation}
	\nabla \Phi = \left(\frac{\partial \Phi}{\partial x},\frac{\partial \Phi}{\partial y}\right) \; ,
\end{equation}
which allow us to convert the image (a~scalar field) into its gradient domain (a~vector field). Then we use 3x3 box filter, as described in \refsec{box_filter}, to smooth both of the partial derivatives matrices ($\frac{\partial \Phi}{\partial x}$ and $\frac{\partial \Phi}{\partial y}$). Finally, the slopes $p$ and magnitudes $M$ of the vector field can be evaluated as follows
\begin{equation}
	p = -\frac{x + y}{x - y} \; ,
\end{equation}
\begin{equation}
	M^2 = x^2 + y^2 \; ,
\end{equation}
where $x$ and $y$ are actually partial derivatives respective to $x$ and $y$. Note that we need a~$\deg{45}$ rotation of the $x,y$ components to express the slope in the Cartesian coordinate system. The negative sign "flips" the array to account for $y$ increasing "downward". Also we need the $q$ parameter of the line's analytical equation for all vectors:
\begin{equation}
	q = y - px \; .
\end{equation}

The last step is to determine the centers of the molecules. This is done by weighted least-squares methods. The weight combines squared magnitude $M^2$ and a~distance from each pixel to the presumed center. The weight is normalized into $[0,1]$ range. The greater the magnitude and the smaller the distance, the greater the weight. Because we work with lines, the minimization can be solved analytically which is much faster than an~iterative solution. All the equations for the analytical solution were published in the supplementary note of \cite{GRADIENTS}.

\subsection{Surface fitting}
\label{sec:surffit}
Probably the most often used localization method is surface fitting. The principle of this method is that a~PSF model is fitted at some point in the input image. Depending on parameters of the fitted model, it is decided whether the result is a~molecule or not. The peak position of the PSF model corresponds to the position of a~molecule.

\subsubsection{PSF models}
\label{sec:psf_models}
As mentioned above, we try to estimate parameters of a~PSF model from an~input image. In \refchapter{image_formation} we described how the image of a~molecule is formed - the molecule is imaged in a~camera not as a~dot but it has a~form of the Airy disk function which is characterized as a~rotationally symmetric intensity distribution with a~peak in the center and with a~surrounding with decreasing intensity converging to zero, see \refsec{diffraction}. This so-called "blob" can be approximated either by the Airy disk itself or by a~simplified mathematical model with its properties similar to the Airy disk. The ultimate goal is to have a~simple model which at the same time guarantees the best localization accuracy possible. By "simple" model we mean a~model with only a~few degrees of freedom. The reason behind this demand is a~simple fact that the estimator operates in a~pixel grid with limited amount of information, therefore the whole problem gets more complex and less robust with the increasing number of parameters.

Moreover, with such a~low amount of information we receive from the camera, all the PSF models presented in the following subsections are almost indistinguishable.

\begin{figure}[h!]
    \centering
    \includegraphics[width=14cm]{images/psf_models}
    \caption[Two-dimensional point spread function examples.]{PSF models: (a)~Airy disk, (b)~2D Gaussian, and (c)~2D parabola. Both the Gaussian and the parabola approximate the Airy disk really well, especially with the consideration of pixelized grid. The parabola in the image has been truncated and flipped over the xz-plane. All the models in the image have been normalized into $[0,1]$ range.}
    \label{fig:psf_models}
\end{figure}

\subsubsubsection{Airy disk}
We have to define the Bessel function prior to defining the Airy disk.
The differential equation
\begin{equation}
	x^2 \frac{\partial^2 y}{\partial x^2} + x \frac{\partial y}{\partial x} + (x^2 - \alpha^2)y = 0 \; ,
\end{equation}
where $\alpha$ is a~real constant, is called Bessel's equation and its solutions are known as Bessel functions. $J_{\alpha}(x)$ and $J_{–\alpha}(x)$ form a~fundamental set of solutions of Bessel's equation for integer $\alpha$. $J_{\alpha}(x)$ is called a~Bessel function of the first kind and it is defined as
\begin{equation}
	J_{\alpha}(x) = \sum_{m=0}^{\infty}{\frac{(-1)^m}{m! \Gamma (m + \alpha + 1)}\left( \frac{1}{2}x \right)^{2m+\alpha}} \; ,
\end{equation}
where $\Gamma(\cdot)$ is Gamma function
\begin{equation}
	\Gamma(x) = (x-1)! \; .
\end{equation}

Now we can define the Airy disk function as an~intensity of the Fraunhofer diffraction pattern \cite{OPTICS_BOOK} of a~circular aperture
\begin{equation}
	I(\theta) = I_0\left(\frac{2J_1(z)}{z}^2\right) \; ,
\end{equation}
where $I_0$ is the maximum intensity of the pattern at the Airy disk center, $J_1$ is the Bessel function of the first kind of order one. Further,
\begin{equation}
	z = \frac{\pi q}{\lambda N} \; .
\end{equation}
Here $\lambda$ is a~wavelength, $q$ is a~radial distance from the optical axis in the observation (or focal) plane and $N=R/d$ ($d$ is an~aperture diameter, $R$ is an~observation distance) is the f-number of the system, i.e., ratio of the lens focal length and the entrance pupil diameter.

From the fitting point of view, the most important parameters are $I_0$ for setting the intensity, $N$ to control the width, and $q$ which can be defined as $q=(x-x_0)^2+(y-y_0)^2$ and used for setting the position ($[x_0,y_0]$).

\subsubsubsection{2D Gaussian}
\label{sec:fitting_gaussian}
One of the commonly used Airy disk simplifying models is a~two-dimensional Gaussian function
\begin{equation}
	G(x,y) = \frac{I}{2 \pi \sigma_x \sigma_y} \exp{- \left( \frac{(x-x_0)^2}{2 \sigma_x^2} + \frac{(y-y_0)^2}{2 \sigma_y^2} \right) } \; ,
	\label{eq:psf_asymmetric_gauss2d}
\end{equation}
where $I$ is the peak intensity, $[x_0,y_0]$ is the peak position, and $\sigma_x,\sigma_y$ are widths of half-axes of the Gaussian parallel to the $x$ and $y$-axes, respectively. With reference to the rotational symmetry of the PSF (Airy disk), the \refeq{psf_asymmetric_gauss2d} can be further simplified by presuming $\sigma_x$ = $\sigma_y$, so the resulting model is
\begin{equation}
	G(x,y) = \frac{I}{2 \pi \sigma^2} \exp{- \frac{(x-x_0)^2 + (y-y_0)^2}{2 \sigma^2} } \; .
	\label{eq:normalized_symmetric_gauss_2d}
\end{equation}

\subsubsubsection{2D parabola}
The most simplified of the three presented models is the two-dimensional parabola:
\begin{equation}
	f(x,y) = I - \left(\frac{x-x_0}{\sigma}\right)^2 - \left(\frac{y-y_0}{\sigma}\right)^2 \; ,
	\label{eq:parabola_2d}
\end{equation}
where $x_0$ and $y_0$ are coordinates of the peak and $\sigma$ determines the width. Similarly as in the Gaussian case, we could distinguish between $\sigma_x$ and $\sigma_y$ but it is not really needed due to the PSF's rotational symmetry.

Furthermore, it is possible to strip the $\sigma$ off the \refeq{parabola_2d} and still be able to find a~sub-pixel location, although with limited accuracy. This leads to an~analytical solution, which is faster and more robust than the iterative one.

\subsubsection{Fitting methods}
All the PSF models mentioned above have several arguments that have to be estimated from input data. There are two common approaches for parameter estimation: minimizing least squares error and maximizing the likelihood function. The methods differ in evaluation of the quality of the fit.

\subsubsubsection{Least squares methods}
Least squares methods seek for a~parameter $\theta$ such that a~difference of input data and a~PSF model is minimized, hence we solve the following optimization problem
\begin{equation}
	\argmin_{\theta} \left\{ \sum_{i=0}^n{\left(y_i - f(x_i | \theta)\right)^2} \right\} \; ,
\end{equation}
where $n$ is the number of samples, $y_i$ is $i$th output sample, and $f(x_i|\theta)$ is an~approximation of $y_i$ calculated from the input sample $x_i$ and estimated parameter $\theta$.

\subsubsubsection{Maximum likelihood estimation}
This method seeks for a~parameter $\theta$ such that an~input image is the most \emph{likely} approximated by a~PSF model. Even though we solve a~maximization problem, it is a~common practice to transform the problem to a~minimization, so we can use standard optimization algorithms. The minimization problem has the form
\begin{equation}
	\argmin_{\theta} \left\{ - \sum_{i=0}^n{\left(y_i \log(f(x_i | \theta)) - f(x_i | \theta)\right)} \right\} \; ,
	\label{eq:mle}
\end{equation}
where $\log$ stands for natural logarithm, $n$ is samples count, $y_i$ is $i$th output sample, and $f(x_i|\theta)$ is an~approximation of $y_i$ calculated from the input sample $x_i$ and estimated parameter $\theta$.

\subsection{Limits of localization methods}
To be able to determine where limits of localization algorithms are, we use tools of estimation theory and statistics. Although it is clear that all the algorithms could be compared one to each other, it is useful to know how good is the performance of a~particular estimator compared to theoretical limits.

\subsubsection{Cramer-Rao lower bound}
We have to define Fisher information prior to defining the Cramer-Rao bound.

Fisher information measures the amount of information that an~observable variable $X$ carries about an~unknown parameter $\theta$, upon which the probability of $X$ depends. Fisher information is defined as the second moment of the partial derivative of the log-likelihood function with respect to $\theta$, i.e.,
\begin{equation}
	\mathcal{I}(\theta) = \mathrm{E}\left[ {\left( \frac{\partial}{\partial\theta} \log{f(X|\theta)} \right)}^2 \middle| \theta \right] = \int{{\left( \frac{\partial}{\partial\theta} \log{f(X|\theta)} \right)}^2 \; f(X|\theta) \; dX} \; ,
\end{equation}
where $E[\cdot|\theta]$ is expectation, i.e., mean of $X$ with given $\theta$, $f(x|\theta)$ is a~likelihood function and $\theta$ is the estimated parameter.

Provided that $f(x|\theta)$ is twice derivable with respect to $\theta$ and under certain regularity conditions, the Fisher information may be seen as a~measure of the curvature of the support curve near the maximum likelihood estimate of $\theta$, see \reffig{curvature}. This can be written as
\begin{equation}
	\mathcal{I}(\theta) = -\mathrm{E}\left[ \frac{\partial^2}{\partial\theta^2} \log{f(X|\theta)} \middle| \theta \right] = -\int{\frac{\partial}{\partial\theta} \log{f(X|\theta)} \; f(X|\theta) \; dX} \; .
\end{equation}

The Cramer-Rao lower bound (CRLB) is defined as reciprocal of Fisher information $\mathcal{I}(\theta)$
\begin{equation}
	\mathrm{CRLB} = \frac{1}{\mathcal{I}(\theta)} \; .
\end{equation}

From the definitions above and from the \reffig{curvature}, it can be deduced intuitively that the CRLB is a~lower bound of the variance of any unbiased estimator
\begin{equation}
	\sigma^{2}_{\hat{\theta}}(\theta) \geq \frac{1}{\mathcal{I}(\theta)} \; \Rightarrow \; \sigma_{\hat{\theta}}(\theta) \geq \sqrt{\frac{1}{\mathcal{I}(\theta)}} \; ,
\end{equation}
where $\hat{\theta}$ is unbiased estimator of $\theta$ and $\sigma$ is the standard deviation.

\begin{figure}[h!]
    \centering
    \includegraphics[width=6cm]{images/curvature}
    \caption[Visual explanation of curvature and the Cramer-Rao lower bound.]{In the figure, there are four Gaussians with the different values of $\sigma$ with the color-coded curvature of the support curves near the Gaussians. As the $\sigma$ gets greater, the curvature gets lower, thus the curvature is indirectly proportional to $\sigma$. Also the greater curvature means the greater Gaussian peak and, of course, the greater value of the Fisher information. All this simply means it is easier to detect a~Gaussian with a~larger peak rather than a~barely visible one.}
    \label{fig:curvature}
\end{figure}

Cramer-Rao's bound is a~lower bound which can not be surpassed by any unbiased estimator. Comparison of an~estimator to such a~bound is a~good measure of the estimator accuracy and its limits for potential improvement.

\subsubsection{Thompson's formula}
Another expression for the prediction of the standard deviation of a~location estimator has previously been published by Thompson et. al. \cite{THOMPSON}. The Thompson formula, as we call it, estimates the localization error for a~given set of imaging and experimental conditions and it is given by
\begin{equation}
    \langle (\Delta x)^2 \rangle = \frac{\sigma^2 + a^2 / 12}{N} + \frac{8 \pi \sigma^4 b^2}{a^2 N} \; ,
    \label{eq:thompson}
\end{equation}
where $\sigma$ is the standard deviation of a~PSF model, $b$ is number of background photons, $a$ is pixel size of the detector in the object space, and $N$ is the number of collected photons.

The Thompson formula has been derived from a~series of experiments analysing the error propagation in the case of a~CCD camera. Hence this formula deals with practical issues specific for single molecule localization microscopy, i.e., image pixelization, camera noise, etc. The formula could be considered to be a~tailor-made lower bound for our purposes because it gives us more realistic insight into the capabilities of estimators, although it was found that this bound does not work properly for EMCCD cameras so the formula must be modified to reflect the electron multiplication process \cite{EMCCD_QUAN} as follows
\begin{equation}
    \langle (\Delta x)^2 \rangle = \frac{2\sigma^2 + a^2 / 12}{N} + \frac{8 \pi \sigma^4 b}{a^2 N^2} \; .
\end{equation}