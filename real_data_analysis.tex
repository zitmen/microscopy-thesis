\chapter{Analysis of real data}
\label{chapter:real_data_analysis}

In this chapter we analyse real data captured by our microscope.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Calibration for 3D localization}
\label{sec:experiment_calibration3D}

Since we equipped our microscope with a~cylindrical lens and we want to analyze some three-dimensional data, we must perform the calibration first.

\subsection{Setup of the calibration}
The calibration data were acquired by an~Andor iXon EMCCD camera a~with resolution of $1004 \times 1002$ pixels cooled to $\deg{-90}$C, with EM gain of $50\times$, and with acquisition time of 110~ms. In total we have recorded sequence of 201 images changing the focal plane position with 10~nm steps. All the images are gray-scale images with 14bit intensity information.

\begin{figure}[h!]
	\centering
	\includegraphics[width=13cm]{images/3d_calibration_data.png}
	\caption[Example of the calibration data.]{Example of the calibration data.}
	\label{fig:calibration_data}
\end{figure}

In the pre-processing phase, we used Gaussian blur with $\sigma=1.3$ pixels and 8-neighborhood maxima detector with threshold set to $0.015$.

For the fitting we used MATLAB function \emph{lsqnonlin} to perform the non-linear least-squares estimation of Gaussian profile according to the \refeq{psf_asymmetric_gauss_rotate}. At first, we ran the fitting routine with initial guess of $\theta=\deg{45}$. From the first pass we estimated the angle $\theta_0=\deg{57}$. Second pass was done with the angle fixed to $\theta=\theta_0$. Subsequently, we grouped the molecules and estimated their $z$-positions in the same manner as described in \refsec{astigmatism}. In addition, we had to deal with the molecules located so far from a~focal plane that they appear as a~cloud of smoke (see \reffig{calibration_data}), which without a~post-processing would devaluate the measurement. Thus we accept only the molecules such that in $z=0$ their $\sigma_1$ and $\sigma_2$ are both less than 3 and $\frac{\sigma_1}{\sigma_2}$ is in range from 0.99 to 1.01.

Note that we use symbols $\sigma_1$ and $\sigma_2$, instead of $\sigma_x,\sigma_y$, because we would have to redefine $x,y$ axes because of the rotation angle. It is in fact unimportant to distinguish between the individual $\sigma$ values, because they are specific to a~particular microscope setup and after the calibration the $\frac{\sigma_1}{\sigma_2}$ ratio will be the same as for the data recorded later.

\subsection{Results}
As mentioned above, the rotation angle of the coordinate system was estimated to be $\deg{57}$. Authors of the method \cite{ASTIGMATISM} skipped this step completely since they managed to place the cylindrical lens so that the half-axes of the ellipses representing molecules were parallel to the axes of the underlying Cartesian coordinate system. This is not case for our data, thus we must perform the angle estimation to be able to precisely estimate the parameters $\sigma_1,\sigma_2$, which are essential for determining the $z$-position. Although the theoretical range of $z$-coordinates is relatively wide (from -2$\mu$m to +2$\mu$m), we cannot use the whole range, because the localization error would be very high. For accurate estimates we are interested only in the part of the range where the ratio $\frac{\sigma_1}{\sigma_2}$ is approximately linear with respect to $z$.

\begin{figure}[h!]
	\centering
	\includegraphics[width=13cm]{images/z2sigma}
	\caption[Results of the calibration.]{Results of the calibration.}
	\label{fig:z2sigma}
\end{figure}

After the estimation of $\sigma_1,\sigma_2$, calculating their ratio, and evaluating the dependency of the ratio and $z$-position, as shown in \reffigx{z2sigma}{b}, we need to derive the inverse dependency, i.e., how to estimate $z$-position based on the $\frac{\sigma_1}{\sigma_2}$ ratio. This can be achieved in many ways, e.g., by interpolation, but we demand to find a~continuous function to model this relationship. Thus we approximated the data by a~polynomial. Relatively good approximation was provided by a~5th~degree polynomial displayed in \reffig{sigma2z}. The polynomial has form of
\begin{equation}
	z = ax^5 + bx^4 + cx^3 + dx^2 + ex + f \; ,
	\label{eq:poly3d}
\end{equation}
where $x=\frac{\sigma_1}{\sigma_2}$ and coefficients ${a=-3467.5}$, ${b=20098}$, ${c=-46385}$, ${d=53589}$, ${e=-31824}$, ${f=7989.3}$. This approach has another advantage for $z$-position estimation as we need to store only five values, instead of all calibration data. Of course, for the application on real datasets it is necessary to limit domain of the polynomial to obtain the most plausible results.

\begin{figure}[h!]
	\centering
	\includegraphics[width=13cm]{images/sigma2z}
	\caption[Calibration data approximated by the 5th degree polynomial.]{(a)~calibration data approximated by the 5th~degree polynomial as defined in \refeq{poly3d}; (b)~residuals of the polynomial approximation relative to the calibration data.}
	\label{fig:sigma2z}
\end{figure}
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Performing the analysis of real data}
\label{sec:real_data_analysis}

In this section we describe a~procedure used for analysis of dataset captured with our equipment. Subject of the analysis are ErbB3 molecules in the membrane of A431 cells. ErbB3 is a~\emph{growth factor receptor} and is important in cancer research. An image of this cell is shown in \reffigx{real_data_analysis}{a}.

The microscope was configured the same as when we were performing the calibration described in \refsec{experiment_calibration3D}, i.e., the data were acquired by an~Andor iXon EMCCD camera with resolution of $1004 \times 1002$ pixels and with the same cylindrical lens placed in front of the objective. A~photo-active area of the CCD chip was restricted by a~window of $515 \times 703$ pixels to eliminate illumination from other parts of the sample. EM gain was set to $150\times$ and acquisition time was 50~ms. Since the configuration is the same as in the calibration, we were able to use our calibration data for determining $z$-coordinates of molecules. Overall, we have recorded 20,000 frames of the cell. The procedure we used and described below is capable of analysis even on very bright images at the beginning of the image sequence when the shutter opens, so we do not discard any usable information.

As a~noise filter we used a $3 \times 3$ box filter followed by a morphological detector with $\mathrm{radius}=7$ and threshold set to $\mathrm{E}(I)+3\sigma(I)$, where E is a~mean value, $\sigma$ is a~standard deviation, and $I$ is set of intensities of all pixels in the analyzed image.

%
% In the cause of use, put this into the theoretical part and run the <filter,detector> experiment to get the results of this method.
%
%As a~noise filter we used a~convolution filter with its kernel equal to inverted Laplacian of Gaussian approximated by Difference of Gaussian (DoG), i.e.,
%\begin{equation}
%	DoG = G(x,y,\sigma_1) - G(x,y,\sigma_2) \; ,
%\end{equation}
%where $G$ is a~two-dimensional Gaussian function defined by \refeq{normalized_symmetric_gauss_2d_I1} and $\sigma_1=1, \sigma_2=2$. This is why the LoG is inverted - because the regular LoG would be approximated with $\sigma_1=2, \sigma_2=1$. The DoG kernel is displayed in \reffig{DoG_kernel}.

%\begin{figure}[h!]
%    \centering
%    \includegraphics[width=12cm]{images/DoG_kernel}
%    \caption[Example of the DoG convolution kernel.]{Example of the DoG convolution kernel (the red line), which was calculated as the difference of the two Gaussians (the blue line and the green line).}
%    \label{fig:DoG_kernel}
%\end{figure}

%For detection we used 8-neighborhood maxima detector with a~simple adaptive threshold, speciffically we detected only the maxima with its value at least $\mathrm{E}(I) + 6\sigma(I)$, where E is a~mean value, $\sigma$ is a~standard deviation, and $I$ is set of intensities of all pixels in the analyzed image.

For localization we used surface fitting with non-linear least-squares error minimization

\begin{equation}
	\argmin_{\{x_0,y_0,I,b,\sigma_1,\sigma_2\}} \left\{ \sum_{x=0}^{w}{\sum_{y=0}^{h}{{\left(I(x,y) - G(\{x,y\} | \{x_0,y_0,I,b,\sigma_1,\sigma_2\}) + b\right)^2}}} \right\} \; ,
\end{equation}
where $G$ is an~elliptic Gaussian function defined by \refeq{psf_asymmetric_gauss_rotate}, $b$ is a~parameter for local background estimation, $[x,y]$ is a~2D position in the fitting region $I$, $\{\sigma_1,\sigma_2\}$ are widths of half-axes of the elliptic Gaussian, and $\{w,h\}$ are fitting region dimensions, i.e., width and height.

In the next step we converted the $\frac{\sigma_1}{\sigma_2}$ ratio to $z$-coordinates according to the conversion polynomial estimated in the calibration step in \refsec{experiment_calibration3D}. Then we eliminated all the $z$-coordinates out of the range $[-400,+400]$~nm.

Finally, we run a~3D rendering method based on a~2D density rendering with $z$-culling technique. All rendered molecules were colorized according to their estimated $z$-coordinates. We also used geometric transformations to rotate the image of the cell. The result is displayed in \reffigx{real_data_analysis}{b-d}. The mapping of the $z$-coordinates is 1 pixel $=$ 15~nm, 1 pixel on the X axis is $\approx$ 78~nm, and 1 pixel on the Y axis $=$ 80~nm.

\clearpage
\thispagestyle{empty}
\begin{figure}[h!]
    \centering
    \includegraphics[width=14cm]{images/real_data_analysis}
    \caption[Results of real dataset analysis.]{We are looking at ErbB3 molecules in the membrane of an A431 cell. (a)~shows the conventional microscope image. (b-d)~are results of the analysis. In (d)~we see a high-resolution image of~(a). In (b)~the image is rotated by $\deg{60}$ around the X axis and in (c)~the image is rotated by $\deg{60}$ around the Y axis. Not only are we able to render the image with a~theoretically unlimited magnification, but we have also recovered the $z$-coordinates of the molecules and thus we see much more detailed structure of the membrane than with the conventional microscope imaging.}
    \label{fig:real_data_analysis}
\end{figure}