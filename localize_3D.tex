\chapter{3D localization of molecules}
\label{chapter:localize_3D}
Development of the localization methods in two-dimensions made a~big leap in the past few years, however, there are still many biological structures that need to be closely examined in all three dimensions for better understanding. That is the reason why many researchers work on techniques to achieve 3D localization.

There are basically three common microscope configurations used to acquire data that can be additionally analysed by computer vision algorithms to determine the positions of molecules in three-dimensional space. The first one is based on astigmatism, the second one uses stereo-vision, and the third one, not discussed in this thesis, is based on interferometry \cite{INTERFEROMETRY}.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Astigmatic imaging}
\label{sec:astigmatism}
This method of 3D localization was introduced in \cite{ASTIGMATISM}. The effect of the astigmatism is achieved by introducing a~cylindrical lens into the optical path inside the microscope, see \reffigx{3d_microscope}{a}. By this reconfiguration of the microscope, we can achieve creation of two slightly different focal planes for $x$ and $y$ directions. As a~result, ellipticity and orientation of a~fluorophore’s projection varies as its position changes in $z$, see \reffigx{3d_microscope}{b}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=10cm]{images/astigmatism}
    \caption[Microscope configuration with cylindrical lens and example of captured data with different z-coordinates.]{(a)~Microscope configuration with the cylindrical lens. (b)~Dependency of the half-axes ratio of the elliptical Gaussian and the distance from the average focal plane ($z$). \emph{Source: \cite{ASTIGMATISM}.}}
    \label{fig:3d_microscope}
\end{figure}

If the fluorophore is located in the average focal plane ($z=0$), the PSF seems to be rotationally symmetric ($\sigma_x = \sigma_y$). On the other hand, if the fluorophore is located above or below the average focal plane, then with increasing distance from the average plane the PSF transforms into an~ellipse with its major half-axis getting larger and the minor half-axis getting smaller. Another observable fact is that if the position of the fluorophore is above the average focal plane ($z>0$), the major half-axis is perpendicular to the major axis of the fluorophore located below the average focal plane ($z<0$). Unfortunately, there is no way to generally determine what is the slope of the major half-axis, because it depends and the particular lens and its exact position relative to the other optical elements in the microscope. This is why we need to do a~calibration first.

The calibration is done in a~few steps. First we need to prepare a~sample with chemical solution containing several sparsely distributed fluorescent beads and place the sample into the microscope to record a~sequence of images. These images are taken with the known focal position, which is continuously changing throughout the entire sequence. After the sequence is recorded, we run a~2D elliptical Gaussian fitting routine on all the images. The result so far is a~list of molecules with its $[x_0,y_0]$ positions, $\sigma_x,\sigma_y$ and image number. Because each image contains exactly the same molecules, the molecules need to be grouped together over the entire image sequence ($z$-stack) using its $[x_0,y_0]$ positions. In the next step, we find the most symmetrical PSF in each of the groups - these are molecules in focus ($z_0=0$). Then for each group, we go through the $z$-stack both above and below the $z_0$ position measuring half-axes of the elliptical Gaussians. Now, we have all the $\sigma_x,\sigma_y$ values and we also know in what $z$-coordinates were these values acquired because the image sequence was recorded with known focal position. Finally, for the localization we collect all the ratios of $\frac{\sigma_x}{\sigma_y}$ grouped by the $z$-coordinates and use the mean values as ground-truth ratios for the particular $z$-coordinates.

Yet the calibration procedure described above would not work correctly. It is because we suppose the half-axes of the ellipses to be aligned with the axes of the underlying Cartesian coordinate system. As mentioned above, it is not generally true, so we have to find out what is the angle of the axes. That is the reason why the fitting is done with the general form of the Gaussian function
\begin{equation}
	G(x,y) = \frac{I}{2 \pi \sigma_x \sigma_y} \exp{- \left( a(x-x_0)^2 + 2b(x-x_0)(y-y_0) + c(y-y_0)^2 \right) } \; ,
	\label{eq:psf_asymmetric_gauss_rotate}
\end{equation}
where
\begin{equation}
	a = \frac{\cos^2{\theta}}{2\sigma^2_x} + \frac{\sin^2{\theta}}{2\sigma^2_y} \; ,
\end{equation}
\begin{equation}
	b = -\frac{\sin{2\theta}}{4\sigma^2_x} + \frac{\sin{2\theta}}{4\sigma^2_y} \; ,
\end{equation}
\begin{equation}
	c = \frac{\sin^2{\theta}}{2\sigma^2_x} + \frac{\cos^2{\theta}}{2\sigma^2_y} \; .
\end{equation}

This means it is necessary to estimate one extra parameter, the $\theta$, which indicates the rotation angle of the Gaussian's major half-axis relative to the coordinate system. The angle is the same for all molecules in the sample. As we also know, a~change of the $z$ causes a~change of the $\frac{\sigma_x}{\sigma_y}$ ratio but not the angle of the axes, therefore the angle $\theta$ is constant. Thus, the only change of the angle happens when we compare the images recorded below and above the average focal plane. However, we know the only thing happening in this situation is the switch of the half axes (major $\leftrightarrow$ minor) which are mutually perpendicular ($\theta$ changes by integer factors of $\frac{\pi}{2}$). Consequently, it is possible to determine only a~single constant angle $\theta_0$ by calculating $E\left[\theta \; \text{mod} \; \frac{\pi}{2}\right]$, where $E$ is mean value and $\text{mod}$ stands for the modulo operator.

At this point we have everything calibrated. The localization is then performed by fitting elliptical Gaussian according to the \refeq{psf_asymmetric_gauss_rotate} with $\theta$ set to $\theta_0$. Finally, using the ratio the the fitted half-axes and with the knowledge of the calibration data we can determine the $z$-coordinate since we know the ratio changes with focus.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Dual objective multi-focal plane microscopy}
\label{sec:3d_biplane}
An~emission from a~sample spreads in all directions, i.e., above and below the sample, what means that even the best objective collects only a~limited amount of light emitted from the sample. To address this problem, a~microscope configuration that uses two opposing objective lenses can be used. One of the objectives is in an~inverted position and the other objective is in an~upright position, see \reffigx{dual_objective_psf3d}{a}. This configuration was introduced in \cite{BIPLANE} and it is called dual objective multi-focal plane microscopy. With this method it is possible to locate molecules with higher accuracy than the previously described methods because we obtain a~larger amount of information.

\begin{figure}[h!]
    \centering
    \includegraphics[width=13cm]{images/dual_objective_psf3d}
    \caption[Biplane microscope configuration and example of three-dimensional point spread functions.]{(a)~Biplane microscope configuration - light from each objective is in fact captured on a~separate halfs of a~single CCD chip in a~camera. \emph{Source: \cite{dMUM}.} (b,c)~Orthogonal projection of 3D PSF models: (b)~Axial Gaussian, (c)~Born \& Wolf. Yellow lines represent the axes ($x=0,y=0,z=20$). \emph{Source: \cite{PSF3D}.}}
    \label{fig:dual_objective_psf3d}
\end{figure}

This method of 3D localization requires use of a~3D PSF. Some of these were described in \cite{PSF3D}. In practice, the Born \& Wolf model shown in \reffigx{dual_objective_psf3d}{c} is often used but for the purposes of this text we describe the localization process with a~simpler PSF, the Axial Gaussian model shown in \reffigx{dual_objective_psf3d}{b}, i.e., 
\begin{equation}
	\mathrm{G3D}(x,y,z) = \frac{1}{\sigma(z)^2} \exp{-\frac{x^2 + y^2}{2\sigma(z)^2}} \; ,
	\label{eq:psf_asymmetric_gauss3d}
\end{equation}
where $\sigma$ is a~linear function of $z$-axis. Note that the process described below is directly applicable to other 3D PSFs by replacing the particular Equations, i.e., \refeq{psf_asymmetric_gauss3d} and consequently Equations (\ref{eq:psf_g3d1}) and (\ref{eq:psf_g3d2}).

Same as the previously described 3D localization method (\refsec{astigmatism}), it is necessary to perform a~calibration first. In this case we want to know the function $\sigma$ and the distance between the two focal planes. Sample preparation and image recording is done exactly the same as it was described in \refsec{astigmatism}. After sequence acquisition, we run the 2D symmetrical PSF fitting for all the images. Then we find out in what $z$-coordinate particular molecules are in focus by looking for the molecules with the least value of $\sigma$. The search of the in-focus molecules runs separately for each focal plane. After it is done, we have all in-focus molecules in both focal planes, thus we can determine the distance between the focal planes by subtracting $z$-coordinates of in-focus molecules in one plane from the in-focus molecules in the other plane and taking the mean value of all the distances. As a~side product we also get information about the $\sigma$ function which is obtained for each focal plane separately ($\sigma_1,\sigma_2$) by tracking a~change of $\sigma$ with a~distance of the depth of field from an~actual $z$-position of the molecules and again calculating the mean value.

There is one more step in the calibration process, that we have not mentioned because we presumed that we know which molecule in one focal plane and which molecule in the other focal plane map to each other. In order to automatically pair the molecules, we must specify a~mapping function $\{f_x,f_y\}$ which maps $[X,Y]$ from one focal plane to the other. This is achieved by selecting a~few molecules as markers in one focal plane and then manually or computationally (e.g., by using the RANSAC algorithm) selecting molecules from the other focal plane corresponding to the markers. Since we already know the $[X,Y]$ positions of the markers, we can determine the mapping function. We presume the mapping function is a~projective transformation which corrects for translation, rotation, scaling and shearing. To finish the calibration we just need to get the parameters of the transformation:
\begin{equation}
	\argmin_P{ \left\{ \sum_{i=0}^n{\left(M^{(2)}_{i} - P \, M^{(1)}_{i}\right)^2} \right\} } \; ,
\end{equation}
where $P$ is the projection matrix, $M^{(1)}_i$, and $M^{(2)}_i$ are positions of $i$th marker in the both top and bottom focal planes.

Eventually, the localization process first runs the 2D PSF fitting to determine $[X,Y]$ molecules positions. Further, with the knowledge of these positions and the transformation matrix $P$, we run the 3D PSF fitting simultaneously for both the top and the bottom focal planes:
\begin{equation}
	\argmin_{z_0}{\left\{ \left[ \int_x{\int_y{ \mathrm{G3D_1}(x,y,z_0) - I_1 + \mathrm{G3D_2}(x,y,z_0) - I_2 }} \right]^2 \right\}} \; ,
\end{equation}
where
\begin{equation}
	\mathrm{G3D_1}(x,y,z_0) = \frac{1}{\sigma_1(z_1-z_0)^2} \exp{-\frac{(x-x_{01})^2 + (y-y_{01})^2}{2\sigma_1(z_1-z_0)^2}} \; ,
	\label{eq:psf_g3d1}
\end{equation}
\begin{equation}
	\mathrm{G3D_2}(x,y,z_0) = \frac{1}{\sigma_2(z_2-z_0)^2} \exp{-\frac{(x-x_{02})^2 + (y-y_{02})^2}{2\sigma_2(z_2-z_0)^2}} \; ,
	\label{eq:psf_g3d2}
\end{equation}
where $[x_{01},y_{01},z_{1}]$ and $[x_{02},y_{02},z_{2}]$ are constant values determined earlier during the 2D PSF fitting ($z_1$ and $z_2$ were evaluated by the calibration functions $\sigma_1$ and $\sigma_2$), $I_1$ and $I_2$ are image regions where the fitting is done, $x$ and $y$ are optimization variables and $z_0$ is the parameter to be optimized.