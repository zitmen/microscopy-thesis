\chapter{Crowded-field problem}
\label{chapter:crowded_field}

One of the challenges in the detection and fitting of molecules is called the crowded-field problem. This occurs when there are two or more photo-activated molecules very close to each other, \reffigx{crowded_field}{a}.

The crowded-field problem causes decrease of the accuracy of the basic fitting methods because they are designed mostly for sparse molecules distribution. For example the Gradient-Field localization method can not be used because, as shown in \reffigx{crowded_field}{b}, the gradients point to the center pixel instead of its two neighbors where the real centers are.

Another example of unsuccessful localization is a~case where two or more molecules are placed right next to each other with their mutual distance less than one pixel, as shown in the \reffigx{crowded_field}{c}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=12cm]{images/crowded_field}
    \caption[Visualization of the crowded-field problem.]{Crowded-field problem - first two images show sum of two Gaussians (since the photons in camera are also summed) placed just two pixels apart which due to a~pixel grid looks like a~single Gaussian. (a)~3D model of the two Gaussians, (b)~gradient-field localization method on the pixelized Gaussians is not able to distinguish the two Gaussians (red crosses mark the real positions of the Gaussians), (c)~shows sum of two Gaussians with its centers inside 1 pixel, but still $\approx 0.78$ pixels apart - all the basic localization algorithms fall short and detect only a~single Gaussian.}
    \label{fig:crowded_field}
\end{figure}

Fortunately, we can use the diffraction phenomenon, described in \refsec{diffraction}, in our favor. Thanks to the surroundings around the centers of molecules, we can roughly estimate how many molecules are in the area and where the centers are (with a~sub-pixel accuracy).

There are several routines for solving the crowded-field problem, e.g., MFA \cite{MFA}, PALMER \cite{PALMER}, CSSTORM \cite{CSSTORM}, or DAOSTORM \cite{DAOSTORM}.

The MFA and PALMER routines use a~common surface fitting method supplemented by fitting a~variable number of PSF models at once. The fitting is followed by selecting the best model, i.e., number of PSFs, determined by log-likelihood ratio test, in case of MFA, or by Bayesian information criterion, in case of PALMER. Moreover, these two routines use a~GPU accelerated computation to optimize their performance. Unfortunately, these accelerated routines were not publicly released, to date.

In the following two sections, we describe the CSSTORM and DAOSTORM routines which are in many ways unique in their approach to the problem and it is though beneficial to analyse the principles which these methods are based on. Furthermore, both these routines are open-source, thus available for our experiments.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{CSSTORM}
\label{sec:CSSTORM}

CSSTORM was introduced in \cite{CSSTORM}. The abbreviation consists of two parts - Compressed Sensing (CS) and Stochastic Optical Reconstruction Microscopy (STORM). The name Compressed Sensing (also called Compressed Sampling) is derived from the capability to reconstruct the original signal even from a~very under-sampled (compressed) measurement of the signal. This algorithm is known in the field of signal processing.

If the original signal is sparse (that is, mostly zeros) or can be made sparse after a~given transformation, compressed sensing can precisely recover signal from highly noisy or corrupted measurements $b$ of the original signal $x$
\begin{equation}
	b = Ax \; ,
	\label{eq:b_eq_Ax}
\end{equation}
where the matrix $A$ is a~known measurement function. If $x$ is sparse, it can be exactly recovered by minimizing its L1 norm $\|x\|_1$ (the sum of the absolute value of each element)
\begin{equation}
	\text{minimize} \; \|x\|_1 \; \text{subject to} \; b=Ax
\end{equation}
even when $b$ has far fewer elements than $x$.

Key to understanding the CSSTORM algorithm is the measurement function represented by the matrix $A$.

The matrix $A$ is determined by a~point-spread function (PSF) of an~imaging system (microscope optics). The $i$th column of $A$ corresponds with the acquired image $b$ such that only a~single molecule is photoactivated and it is located at the $j$th position in $x$. The goal is to get the original signal $x$ from the measured signal $b$ and known measurement function $A$ with the assumption of the signal $x$ being both positive and sparse.

The $x$ represents the resulting high-resolution image, which can be arbitrarily oversampled. The $x$ is a~vectorized discrete grid with a~spacing smaller than the camera pixel size. \reffig{csstorm} shows the meaning of particular elements of the \refeq{b_eq_Ax}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=14cm]{images/csstorm}
    \caption[Visual explanation of the basic principle of the compressed sensing method.]{Visualization of the \refeq{b_eq_Ax}, where $b$ is an~image acquired from a~camera (there are 4 Gaussians with $\sigma=1.3$ in the figure), $A$ is the measurement matrix with its $i$th column showing an~image $b$ such that only the single molecule at $i$th position is photoactivated, and $x$ is the recovered high-resolution signal (green marks represent centers of the detected molecules). Since all the PSFs  in the matrix $A$ are known (Gaussian with $\sigma=1.3$), it is possible to detect all four molecules. In contrast, any basic surface fitting algorithm would detect only one molecule with $\sigma \approx 1.6$.}
    \label{fig:csstorm}
\end{figure}

With the PSF known, compressed sensing can recover the fluorophore positions even with extensive spot overlap in $b$. Taking into consideration that the measurement process is inherently noisy, the L1 norm minimization is constrained as follows
\begin{equation}
	\text{minimize} \; \|x\|_1 \; \text{subject to} \; \|Ax-b\|_2 \leq \epsilon (\Sigma b_j)^\frac{1}{2} \; ,
\end{equation}
where $\|\cdot\|_2$ stands for L2 norm, $\epsilon^2$ sets the maximum ratio between the sum of squared deviations (the L2 norm), and $\Sigma b_j$ is the sum of variances of the photon shot noise in $b$.

Many different mathematical formulations have been proposed in the literature to implement the L1 norm minimization for compressed sensing. Specifically, authors of the CSSTORM algorithm used the following formulation:
\begin{equation}
	\text{minimize} \; c^Tx \; \text{subject to} \; x_i \geq 0 \; \text{and} \; \|Ax-b\|_2 \leq \epsilon (\Sigma b_j)^\frac{1}{2} \; ,
\end{equation}
where the weight vector, $c$, is to account for the difference of the total contribution to the camera image from one fluorescent molecule at different locations. The value of the $i$th element of $c$ equals the summation of the $i$th column of $A$. The minimization term, $c^Tx$, is equivalent to a~weighted L1 norm of the original signal $x$, because the signal $x$ is non-negative.

To account for the background of the image, one additional element was introduced in $x$. The corresponding element in $c$ is set to $0$, and all elements in the corresponding column of $A$ are set to $1$. In this case, the value of this extra element in $x$ represents a~uniform image background, with no imposed sparsity constraint.

Although it is possible to solve the optimization problem for the whole image at once, it is not very practical, because the matrix $A$ and thus the vectors $c$, $x$, and $b$ would be large and the optimization problem would become very complex and much more time demanding. Therefore a~sliding-window approach is used. Here the window slides through the whole image and the optimization problem is solved only for the image segment underlying the window, thus the computation runs a~lot faster. The result is not identical to the result we would get by solving for the whole image at once but it is still a~good approximation with only limited loss of accuracy, especially if the windows have mutually overlapping boundaries.

CSSTORM can be extended for fitting in 3D. This can be achieved by appending additional columns to the matrix $A$ (thus the vector $c$) and to the vector $x$ representing PSFs for different $z$-coordinates, i.e., with larger values of $\sigma$ or with asymmetrical PSFs scaling the $\sigma_x,\sigma_y$ parameters, see \refchapter{localize_3D}. The \refeq{b_eq_Ax} then transforms in
\begin{equation}
	b = (A_{z_1} \; A_{z_2} \; ... \; A_{z_n})(x_{z_1} \; x_{z_2} \; ... \; x_{z_n}) \; ,
\end{equation}
where the matrices and vectors inside of the parentheses are vertically concatenated together.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{DAOSTORM}
\label{sec:DAOSTORM}

DAOSTORM \cite{DAOSTORM} is a~Stochastic Optical Reconstruction Microscopy (STORM) method directly derived from the DAOPHOT \cite{DAOPHOT} project which is a~stellar photometry (PHOT) software package written by Peter Stetson at the Dominion Astrophysical Observatory (DAO).

DAOSTORM authors simply used the fact that SMLM data are very similar to astronomical data for which the DAOPHOT was designed. Thus DAOSTORM is nothing else but a~direct call of routines in the DAOPHOT package (or specifically a~Python wrapper because the original package is a~FORTRAN77 legacy code written in 1987).

Without any further ado, the crowded-field localization part of the algorithm proceeds in these few steps:
\begin{algorithm}
\caption{DAOSTORM}
\label{alg:DAOSTORM}
\begin{algorithmic}[1]
    \State Local maxima in the image are identified as candidate molecules.
		\State Multiple PSFs are fit to the image to produce initial localizations.
		\State The residuals image is inspected for molecules left out of the initial fit. The positions of these molecules are added to the list of localizations from step 2.
		\State Multiple PSFs are fit to the original image, using updated list of candidate molecules from step 3. This yields a~more accurate fit compared to results in step 2.
		\State Steps 3-4 are repeated 4 times to maximize the recall (fraction of detected molecules). The final data show high recall and localization precision.
\end{algorithmic}
\end{algorithm}

\begin{figure}[h!]
    \centering
    \includegraphics[width=13cm]{images/DAOSTORM}
    \caption[DAOSTORM schematic.]{DAOSTORM schematic. The individual numbered images directly correspond to the steps of the \refalg{DAOSTORM}. \emph{Source: supplementary note of~\cite{DAOSTORM}.}}
    \label{fig:DAOSTORM}
\end{figure}

The algorithm as presented above is fairly superficial. It explains the main idea of solving the crowded-field problem, however, the deeper understanding of the algorithm is advantageous since some of the used principles are unique, compared to the other algorithms we were studying.

At first, the algorithm seeks for star candidates  (or in our case molecule candidates). Due to the presence of noise, non-uniform background, nebulae, out-of-focus stars and other distortions found in the image, the application of the convolution filter is inevitable, see \refsec{lowered_truncated_gaussian}. When the filtering is done, a~maxima detector is applied on the image resulting in the list of candidates. Unfortunately, the list might contain a~lot of false-positive detections, so Stetson invented two criterion to eliminate such candidates. The first, sharpness criteria, evaluates a~dependency of pixels' intensities and its distance from the center of a~molecule. The other criteria is called roundness, which is a~measure of how symmetrical ($\frac{\sigma_x}{\sigma_y}$) is a~fitted 2D Gaussian function, see \refeq{psf_asymmetric_gauss2d}.

Before the fitting starts, it is necessary to estimate a~PSF model. DAOPHOT, unlike the other methods, uses a~combination of both the analytical and also empirical PSF to achieve a~better localization accuracy because of the simple fact that parameters of the used equipment does not fully match an~ideal mathematical model. The analytical PSF is, as usual, presented by 2D Gaussian. The empirical part of the PSF is estimated in the following way. Several of the brightest molecules are chosen from the list of candidates and the analytic PSF is fitted on them which gives us their sub-pixel positions. The analytic PSF model with its estimated parameters is then subtracted from the input image to get a~residual image. At this point the parts of the residual image where the molecules were fitted have to be aligned in a~common grid with its centers put one on each other and residual intensities interpolated at the grid intersections. These interpolated values are then called profile corrections and the grid is then used as the empirical part of PSF. The more bright stars we use to generate the empirical PSF, the better signal-to-noise ratio is and therefore we get a~better estimate of the empirical PSF.

In the fitting phase, an~estimator seeks for the parameters with the least square error given a~PSF as sum of the analytical PSF and interpolated profile corrections.

\begin{figure}[h!]
    \centering
    \includegraphics[width=4cm]{images/DAOSTORM_PSF.png}
    \caption[DAOSTORM - corrections of the analytical PSF.]{DAOSTORM - corrections of the analytical PSF to the actual, observed stellar (molecular) profile.}
    \label{fig:DAOSTORM_PSF}
\end{figure}

There is still one important part of the algorithm missing - the post-processing. After each step of fitting several molecules the algorithm checks whether a~two or more molecules are at distances less than 0.4 FWHM (Full-Width at Half-Maximum). If so, all the molecules except one are removed. At the same time fitting errors are checked to be below a~threshold. Thanks to these measures, false-positive detections are limited.

Since the DAOPHOT is a~legacy code, this method is not easily extendable for 3D localization. This could be achieved by omitting the calculation and usage of correction matrix because it carries information about only a~single PSF. Also the criteria for candidate selection (roundness, sharpness) would have to be either redesigned or skipped. Nevertheless, the main idea of the DAOPHOT is, of course, universal and can be further extended.