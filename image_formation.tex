\chapter{Image formation}
\label{chapter:image_formation}

To be able to achieve the best results possible during the image analysis, it is crucial to fully understand the process of image formation, especially in field of super-resolution microscopy since all the components operate on the boundaries of their construction limits.

\begin{figure}[h!]
    \centering
    \includegraphics[width=11cm]{images/microscope.png}
    \caption[Single-molecule super-resolution basic microscope configuration.]{Single-molecule super-resolution basic microscope configuration. \emph{Source: \cite{PALM_INTRO}.}}
    \label{fig:microscope}
\end{figure}

As shown in \reffig{microscope}, an~image is captured as follows: the light from a~laser passes through a~set of filters, reflects from a~dichroic mirror, then is focused by an~objective lens into a~sample. Molecules in the sample become photo-activated and emit a~visible light. The light is collected by the objective lens, then passes through the dichroic mirror, then through an~emission filter and imaging lens right into an~EMCCD (Electron Multiplying Charge Coupled Device) camera. In the camera photons are converted to an~electric charge, digitized, and saved into the memory of the computer that controls the camera.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Fluorescence microscopy}
\label{sec:fluorescence_microscopy}
A~key concept in the fluorescent microscopy is the fluorophore - a~substance capable of absorbing light of a~certain wavelength which then emits light of a~longer wavelength (explained in \refsec{fluorescence}). Light of different wavelengths can be separated using filters, so only the emitted signal on a~black background can reach the eyepiece or camera (exciting radiation is filtered out), which significantly increases sensitivity of the equipment. Fluorophores are detected using a~fluorescence microscope~-~an~instrument which is very similar to a~conventional optical microscope. The difference is addition of a~very strong light source and two types of filters. The first set of filters enables individual fluorophores to be excited by light of selected wavelengths, the second set of filters eliminates all but the light emitted by fluorophores.

In the application of fluorescence microscopy in living cells the Green Fluorescent Protein (GFP) is often used. This protein is part of the genome of \emph{Aqueoria victoria} jellyfish. The protein can be isolated, adjusted, and appended into the genetic information of particular protein in a~cell, which we are interested in. Thus we introduce the modified gene back into cells and the protein becomes synthesized with a~"shining" mark thanks to which we can track a~location and dynamics in the cell. Nowadays we are not limited to just one color - several colors were derived from the GFP and genes of other species, such as corals \cite{GFP_COLOR}. Side by side in one cell, we can introduce several gene constructs encoding several different color-coded proteins and simultaneously monitor their fate in the cell.

A~disadvantage of fluorescence microscopy is that an~eye (or camera) also detects part of light signal above and below the plane of focus. Another complication of fluorescence microscopy is that the fluorophores are decomposed due to an~intense radiation and lose their ability of absorption and emission - the so-called photo-bleaching. When working with live cells it is essential (especially for long-term observation) to limit their exposure to radiation from high-energy light sources. This can be achieved by using shutters letting the excitation light to reach a~specimen only during exposure, and by using the most sensitive cameras or photomultiplier tubes.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\subsection{Fluorescence principle}
\label{sec:fluorescence}
Fluorescence microscopy utilizes properties of molecules to absorb light.The shorter the wavelength, the higher its energy. This energy is absorbed by electrons inside of the fluorescent molecules. Electrons exist in a~molecule at a~certain stable level of energy, i.e., the ground-state. After light is absorbed by a~molecule, it moves to a~higher energy level. Since the electrons on this energy level are unstable, they tend to return to the ground-state. This causes the release of extra energy partially in the form of heat and partially in the form of light. The fluorescence principle is shown in Jablonski diagram in \reffigx{jablonski-stroke}{a}. Since a~part of the emitted energy is released in the form of an~invisible radiation, such as heat, the visible light has lower energy, i.e., longer wavelength than the one originally delivered to the molecule. This phenomenon is called Strokes shift and it explains why emitted radiation wavelengths are longer, i.e., have less energy than the wavelengths of the excited radiation, see \reffigx{jablonski-stroke}{b}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=11cm]{images/jablonski-stroke.png}
    \caption[Jablonski diagram and Strokes' shift diagrams.]{(a)~Jablonski diagram showing the electron energy levels, (b)~Strokes shift, which is the difference between positions of the band maxima of the absorption and emission spectra of the same electron transition. \emph{Sources: \cite{ELECTRON_EXCITATION_EMISSION} and Wikipedia (Fluorescence, Strokes shift).}}
    \label{fig:jablonski-stroke}
\end{figure}
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Diffraction of light}
\label{sec:diffraction}
As light from a~sample passes through an~optical system of a~microscope, the diffraction phenomenon affects the image formation process. Because of the diffraction, a~resulting image of a~point source of light does not look like an~ideal point but it has the shape of an~Airy disk. The Airy disk shape is caused by combination of the diffraction and the Huygens' principle - as a~light wave is refracted by the curved surface of a~lens, new (virtual) sources of the wave are formed behind the lens. Those "new" waves are mutually phase-shifted since each of them has a~different source. As the waves interfere, see \reffigx{airy-diffraction}{a}, the molecule is projected as the Airy disk, see \reffigx{airy-diffraction}{b}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=14cm]{images/airy-diffraction.png}
    \caption[Diffraction effect and Airy disk projections.]{(a)~Demonstration of phase-shifted waves interference after the diffraction on a~lens, (b)~the resulting point appearance - the Airy disk (1D, 2D, and 3D). \emph{Sources: \cite{OPTICS_OF_IMAGE_FORMATION,DIFFRACTION_OF_LIGHT}}}
    \label{fig:airy-diffraction}
\end{figure}

\subsection{Point Spread Function}
As already mentioned, the Point Spread Function (PSF) describes the response of an~imaging system to a~point object, i.e., how the light originating from a~point source is spread. In the microscope, where we work with two-dimensional images, the point appears as an~Airy disk.

The PSF is specific to a~particular microscope configuration and can be either calculated analytically or estimated experimentally. Once we know the PSF, it can be applied to all images acquired with the same microscope configuration. Mathematical PSF models and their parameters are described in \refsec{psf_models}.

With the concept of PSF we also use an~important notion of Full-Width at Half-Maximum (FWHM). This says how wide a~PSF is at the half of its maximal intensity as shown in \reffig{fwhm}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=5.5cm]{images/fwhm}
    \caption[Full-Width at Half-Maximum of a~Gaussian function.]{Full-Width at Half-Maximum of a~Gaussian function. \emph{Source: Wikipedia (Full width at half maximum).}}
    \label{fig:fwhm}
\end{figure}
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Capturing the image}
\label{sec:capturing_the_image}
To be able to computationally process microscope images, there must be a~camera on one end of the optical path within the microscope. In our case, the data were acquired by an~EMCCD (Electron Multiplying Charge Coupled Device) camera.

\subsection{EMCCD camera}
A~CCD (Charge Coupled Device) image sensor is a~two-dimensional array of p-doped MOSFET capacitors.

When the camera shutter opens, the arriving photons excite photo-electrons in the detector, so the two-dimensional array captures a~two-dimensional picture corresponding to a~scene projected onto a~focal plane of the sensor. As soon as the (electronic) shutter closes, the control electronics make all the excited electrons move to a~storage area, where each capacitor starts to shift its content to its neighbor as shown in \reffig{emccd}. When the charge reach end of a~column it moves to another shift register, which moves the electrons through a~high voltage electron multiplying (EM) register where the electrons are multiplied using the avalanche effect. Output of the EM register is connected to an~output amplifier and further into an~analog-to-digital converter, where voltages are digitized and sent to memory.

\begin{figure}[h!]
    \centering
    \includegraphics[width=12cm]{images/emccd}
    \caption[Principle of an~EMCCD camera.]{Figure (a)~shows the image capture area and storage area. Thanks to this arrangement, photon detection and saving the data can be done simultaneously which leads to higher frame rate and also renders a~physical shutter unnecessary. Figure (b)~shows principle of MOSFET capacitors inside of a~CCD chip. After excitation of photo-electrons, voltage is systematically applied on the electrodes to move the photo-electrons to the output. \emph{Source: Wikipedia (EMCCD, CCD).}}
    \label{fig:emccd}
\end{figure}

The main advantage of EMCCD cameras is that the signal is amplified in the EM registers, so the read-out noise affecting the signal is relatively low compared to the amplified signal. Hence the read-out noise of EMCCD cameras is considered to be negligible.

EMCCD cameras indispensably need a~cooling system to cool the chip down to temperatures in the range of $-65^{\circ}$C to $-95^{\circ}$C \cite{ANDOR_MANUAL}, because with the raising chip temperature dark currents get more significant and cause another source of a~noise.

\subsection{Sources of image degradation}
Unfortunately, the captured image will never be perfect, because there are many negative effects influencing the signal during its journey from the sample to the memory, where it is stored.

\begin{itemize}
		\item Diffraction of light from the sample resulting in the Airy disk response as described in \refsec{diffraction}.
		\item Dust or any other dirt on any of the lens and/or filters in path of the light in the microscope or on the CCD chip.
		\item Any undesirable light from the environment where the measurement is performed.
		\item Signal discretization, i.e., we don't know where a~particular photon landed inside of a~single pixel sensor.
    \item Shot noise which consists of random fluctuations of the electric current originating from the fact that the current actually consists of a~flow of discrete charges (electrons). The electrons arrive with a~Poisson distribution \cite{FITTING}.
    \item Salt and pepper noise which represents itself as randomly occurring white and black pixels. This kind of noise often originates in quick transients or any interferences from sources of electromagnetic signal radiation.
    \item Dark current is caused by thermal emission of charge carriers, which are ever present in any semiconductor device. This is minimized by cooling the CCD chip, because with higher temperatures, the dark current level raises.
		\item Multiplication noise - when a~signal electron is subjected to a~gain of $\times 300$ for example, it does not follow that every electron inputted into the gain register will be amplified to exactly $300$ electrons. There will in fact be a~distribution of output signal with a~mean value of $\times 300$ the input signal \cite{EMCCD_TUTORIAL}.
		\item The flat field problem arises because the camera does not detect strictly the photons emitted from the current focal plane, but it also detects photons emitted by out-of-focus molecules. This creates a~non-uniform background. Non-uniform illumination can also result in images with un-even backgrounds.
		\item Saturation - if a~pixel is illuminated by a~bright particle and/or if the exposure time is long enough, the pixel will fill up and the electrons will start to fill the neighboring pixels. When the output image is read, all the extra electrons will be spread over the column containing the saturated pixels, so called "blooming".
		\item Offset of the camera - since the MOSFETs in the CCD chip have to be biased, even a~black pixel without any photons detected does not contain zero electrons. The problem is worse in EMCCDs since the distribution of gain is so wide.
\end{itemize}

\begin{figure}[h!]
    \centering
    \includegraphics[width=12cm]{images/image_degradations}
    \caption[Examples of image degradations.]{Examples of image degradations: (a)~an~image afflicted by the diffraction effect, corrupted by (b)~an~additive noise, and by (c)~an~un-uniform background.}
    \label{fig:image_degradations}
\end{figure}

If it was a~general use of a~camera, we could ignore almost all of these degradations, however, we work with very dark images with low light conditions, so called "low light imaging". This makes the entire process much more complicated, because all of these distortions inflict the further analysis.