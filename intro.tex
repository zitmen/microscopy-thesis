\begin{introduction}

This project builds on previous work done by Dr. Guy Hagen, Ph.D., and Ing. Pavel Křížek, Ph.D., from the Institute of Cellular Biology and Pathology at the First Faculty of Medicine, Charles University in Prague in the field of super-resolution imaging in fluorescence microscopy \cite{ORIGINAL}.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Motivation}
Conventional optical microscopy (or light microscopy) has been used for many decades as one of the main tools for biologists to study living objects. Further progress required technological innovations that would allow to overcome the limitations - distinguishability of objects smaller than the shortest wavelength of visible light.

Directly related to this issue is the important notion of resolution, which has more importance than microscope magnification. The resolution is defined as the least possible distance of two different points not to be detected as one, see \reffig{resolution}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=7cm]{images/resolution.png}
    \caption[Airy patterns and the limit of resolution.]{Airy patterns and the limit of resolution. \emph{Source: \cite{RESOLUTION}.}}
    \label{fig:resolution}
\end{figure}

The necessary calculations for the resolution limit of a~microscope were derived by German physicist, pioneer in the field of microscopy, Ernst Karl Abbe (1840 - 1905), who found that light with wavelength $\lambda$ traveling in a~medium with refractive index $n$ and converging to a~spot with angle $\alpha$ will make a~spot with radius:
\begin{equation}
    \delta = {\lambda \over {n \times \sin{\alpha}}}
    \label{eq:abbe}
\end{equation}

The resolution of a~microscope is expressed by the denominator of the \refeq{abbe}, so-called Numeric Aperture ($NA = n \times \sin{\alpha}$, see \reffig{na}). The numeric aperture determines what are the limits of an~effective magnification and it also affects a~depth of field (DoF). Depth of field means the thickness of a~specimen in which investigated objects are located. Objects lying out of DoF appear to be blurred or do not appear at all. The smaller NA is, the thicker layer of specimen is in focus. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=8cm]{images/numericalAperture.png}
    \caption[Explanation of Numerical Aperture.]{The figure illustrates a~series of light cones derived from objectives of varying focal length and numerical aperture. As the light cones grow larger, the angular aperture ($\alpha$) increases from $7^{\circ}$ to $60^{\circ}$, with a~resulting increase in the numerical aperture from $0.12$ to $0.87$. The higher the numerical aperture of a~lens, the better the resolution of a~specimen obtained by that lens. With the higher numerical aperture we get more light into a~lens producing brighter images. We can directly compare microscope objectives by comparing their F-number, i.e., $F=10^4(NA^2/mag)^2$, where $mag$ stands for magnification \cite{VIDEO_MICROSCOPY_BOOK}. \emph{Source: \cite{NICON_NA}.}}
    \label{fig:na}
\end{figure}

In the context of bypassing the diffraction limit, we talk about super-resolution microscopy. Breakthrough discovery on bypassing the diffraction limit was made by fluorescence microscopy. There are many applications of the fluorescent microscopy, but in this thesis we specifically concentrate  on single molecule localization microscopy that exploits the fact that the individual molecules bound with a~fluorescent dye start randomly blinking after they were exposed to radiation from a~light source. It is very unlikely the molecules would be photoactivated all at the same time, thus we get an~image of sparsely distributed photoactivated molecules. This allows us to accurately localize the individual molecules and by that surpass the diffraction limited so we can localize molecules with approximately 20~nm precision, unlike the conventional light microscopy which has its resolution limit at approximately 250~nm.

Since the molecules have to be somehow detected, a~crucial part is usage of an~image analysis software that enables analyzing the microscope images and rendering high-resolution images and 3D models of observed objects. In this thesis we focus on the common challenges of the single molecule localization microscopy, i.e., detection and localization of molecules, rendering high-resolution models, solving crowded-field problem, and estimating z-coordinate of detected molecules.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Project goals}
The goal of this project is to improve the existing software solution to detect as many molecules as possible with the error kept at minimum. The current solution suffers mainly because there is not enough molecules detected to create high-quality models of the examined cells. We concentrate not only on basic techniques of molecules detection and localization, but also on phenomena called crowded-field, which is quite complex problem arising when two or more molecules are so close to each other that they blend together and it appears like there is only a~single molecule. Another challenge we have to deal with is a~localization in all three dimensions which is a~hot topic in today's microscopy and it is essential for us be able to create realistic 3D models of examined biological structures. That is why we also put our efforts to analyses and evaluation of several different data visualization algorithms to get the best high-resolution models possible.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Thesis outline}

This thesis is structured as follows:
\begin{itemize}
    \item \refchapter{image_formation} provides a~view inside of the image formation in a~microscope, i.e., how the light comes into existence in the sample then how it propagates through the optics of microscope up to the camera, where the photons are collected and the image is created.
    \item \refchapter{image_analysis} explains principles of image analysis methods used in the experimental part of this thesis (\refchapter{experiments}).
    \item \refchapter{localize_3D} describes two common approaches to single molecule localization in three-dimensions.
		\item \refchapter{crowded_field} defines a~phenomena called the crowded-field problem, which makes the basic localization methods ineffective, because they leave many molecules undetected.
		\item \refchapter{data_visualization} describes data-visualization methods for rendering the high-resolution models of examined biological objects.
		\item \refchapter{experiments} presents conducted experiments and investigates the suitability of the methods for practical use based on the obtained results.
		\item \refchapter{real_data_analysis} presents results of analysis and rendering of real data captured by our microscope.
    \item Finally, in the conclusion we declare the most suitable process of analysis and visualization for our data and we outline future research.
\end{itemize}
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\end{introduction}