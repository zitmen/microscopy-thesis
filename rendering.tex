\chapter{Data visualization}
\label{chapter:data_visualization}

As mentioned in the Introduction, visualization (or rendering) is one of the variants of processing the results of the localization process. The main goal is to get high-resolution models suitable for research of biological structures.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{Time-projection}
The simplest possible visualization method is a~time-projection since the data were acquired during certain period of time.

Input of a~time-projection are raw images from a~camera. The whole process of molecules localization is skipped, so this is one of the fastest ways to get a~picture of the acquired data. It is therefore evident that this method does not give us a~good quality high-resolution model of an~examined object.

\subsection{Maximum intensity time-projection}
For each single pixel this method looks for maximal intensity throughout the entire image sequence and writes the maximum into the output image at the pixel's position.

\subsection{Standard deviation time-projection}
For each single pixel this method calculates standard deviation of intensities throughout the entire image sequence and writes the result into the output image at the pixel's position.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{2D rendering algorithms}
\label{sec:rendering_2d}
Since time-projection does not provide sufficient resolution in the resulting image, it is necessary to use more advanced methods.

The presented rendering algorithms require a~list of molecules as their input. The list can be an~output of any of the localization techniques described in this text, thus this process is more time consuming than a~time-projection. Nevertheless, this way we can render an~output image of virtually any size. Of course, the quality depends on the localization method accuracy and also on the selected rendering algorithm.

The output of any rendering algorithm presented in this chapter is a~matrix of real numbers normalized to $[0,1]$ range that represents pixels' intensities. Since each pixel has a~single intensity value, the result is a~gray-scale image, however, it is possible to map colors to the images using the HSV (Hue-Saturation-Value) color space with the Saturation and Value set to $1$ and Hue set to the pixel intensity.

\subsection{Scattergram Visualization}
\label{sec:scattergram}
The simplest of the rendering methods is one called a~scatter plot, see \reffigx{scatter_vs_histogram}{a}. It simply sets all pixels' intensities to $0$ and then sets the pixels where a~molecule is located to $1$.

\subsection{Histogram Based Visualization}
Histogram Based rendering, as shown in \reffigx{scatter_vs_histogram}{b}, calculates a~2D histogram of the positions of the molecules with a~chosen square bin size and display this as a~pixelated image whose local intensity is proportional to the bin occupancy.

\begin{figure}[h!]
    \centering
    \includegraphics[width=13cm]{images/scatter_vs_histogram}
    \caption[Scattegram rendering and histogram rendering.]{The difference between (a)~scattergram and (b)~histogram visualization. Blue markers show the real positions of the molecules in the pixel grid. Those are then converted to the pixels' intensities.}
    \label{fig:scatter_vs_histogram}
\end{figure}

\subsection{Density Based Gaussian Visualization}
A~little more advanced method draws a~2D Gaussian with its center at a~position of a~molecule and with its standard deviation proportional to localization accuracy (the formula is exactly the same as \refeq{normalized_symmetric_gauss_2d} with intensity $I$~set to $1$).

\begin{figure}[h!]
    \centering
    \includegraphics[width=13cm]{images/density_vs_gaussian}
    \caption[Density based rendering and Gaussian based rendering.]{Comparison of (a)~Density based visualization and (b)~Gaussian based visualization. In each figure, there is a~pixel grid with molecules located with a sub-pixel precision on the left and a~rendered picture on the right.}
    \label{fig:density_vs_gaussian}
\end{figure}

\subsection{Gaussian Based Visualization}
This method is almost the same as previous one, but with the difference that the Density Based Gaussian renders all blobs with the constant intensity $I=1$ while this method sets the intensity to the magnitude of the detected molecules, i.e., the formula is identical with \refeq{normalized_symmetric_gauss_2d}.

\subsection{Quad-Tree Based Adaptive Histogram Visualization}
Quad-Tree (QT) based rendering is similar to Histogram based rendering. Instead of a~constant bin size, the QT method is adaptive with respect to the bin size, i.e., each bin can be a~different size. That is why Quad-Tree is used to determine the bin size. A~quad-tree is defined as a~tree data structure in which each internal node has exactly four children. Within this method the QT is used as follows:

\begin{algorithm}
\caption{Calculation of QuadTree}
\label{alg:quadtree}
\begin{algorithmic}[1]
    \If{MoleculesCount(node) $\geq 4$}
        \State new\_nodes $\leftarrow$ Divide(node)
        \ForAll{new\_node \textbf{in} new\_nodes}
            \State QuadTree(new\_node)
        \EndFor
    \EndIf
\end{algorithmic}
\end{algorithm}

The algorithm divides an~input space into four parts (nodes) until each node has less than four molecules in it. Root node contains the whole input image, so the initial call is \emph{QuadTree(image)}. The result is a~structure similar to a~histogram with different bin sizes, see \reffigx{quadtree_vs_triangulation}{a}. The intensity of a~pixel inside of the area belonging to any node is proportional to the number of molecules in the node divided by the node area.

\subsection{Delaunay Triangulation Based Visualization}
\label{sec:delaunay}
Delaunay triangulation (DT) for a~set $P$ of points in a~plane is a~triangulation $DT(P)$ such that no point in $P$ is inside the circumcircle of any triangle in $DT(P)$.

\begin{figure}[h!]
    \centering
    \includegraphics[width=13cm]{images/quadtree_vs_triangulation}
    \caption[Quad-Tree based rendering and Delaunay based rendering.]{Examples of building (a)~Quad-Tree, and (b)~Delaunay Triangulation. }
    \label{fig:quadtree_vs_triangulation}
\end{figure}

This geometrical structure can be used for rendering in the following way. First calculate $DT(P)$ given the list of molecules $P$. This yields a~list of triplets, each representing one individual triangle. Members of a~triplet are vertices of a~triangle. In the output image, these triangles are filled with intensity $I$ equal to reciprocal of the triangle area:
\begin{equation}
	I = \frac{1}{\sqrt{s(s - a)(s - b)(s - c)}} \; ,
\end{equation}
\begin{equation}
	s = \frac{a + b + c}{2} \; ,
\end{equation}
where $a$, $b$, $c$ are lengths of the triangle edges.

Since computing $DT(P)$ is a~complex operation, we have chosen the fast Geoff Leach's improved implementation of Guibas-Stolfi algorithm ($O(n\log{n})$) \cite{TRIANGULATION}.

DT based rendering slightly differs from the previous methods in the drawing part. Previously described methods always colored single pixels or simple shapes (squares, rectangles). In contrast, DT rendering has to use a~rasterization algorithm to draw individual triangles. We have chosen a~commonly used algorithm for polygon rasterization called Scan Conversion \cite{GRAPHICS_BOOK}.

\subsection{Jitter \& average technique}
In \cite{RENDERING} was described a~technique which allows us to get rid of sharp edges in rendered images. The main idea is to render more than one image and in each of these images the points are shifted to a~random direction with a~random distance from its original positions (jittering). The shifting distance is defined by the estimate of localization error. Eventually, for each pixel we calculate the mean value throughout all the rendered images and save this value to the final image (averaging). Consequently the final image has less sharp edges, which makes the look of the image more natural and it also reduces possible artifacts caused by uncertainty of the localization process.

This technique can be applied to all the methods described in this chapter.
% ============================================================================================ %
% ============================================================================================ %
% ============================================================================================ %
\section{3D rendering algorithms}
Apparently the simplest method for 3D data visualization is ignoring the $z$-coordinate and render a~2D image.

However, ignoring the third dimension in the further analyses would degrade the effort invested in retrieving the 3D positions of the molecules.

The most direct method, which is often used, is to display data in two dimensions with the third dimension encoded in colors used for the rendered particles. That is, create the image in HSV (Hue-Saturation-Value) color-space. Then use the scattergram visualization and setting $H$ to the normalized $z$-position as follows
\begin{equation}
	H = \frac{z - z_{min}}{z_{max} - z_{min}} \; ,
\end{equation}
with $S=1$ and $V=1$.

Even though this approach preserves information about the third dimension it is still not a~regular 3D model that could be examined from different angles. For these purposes it is necessary to use techniques known from computational geometry and computer graphics described in section \refsec{developing_3d_rendering_methods}, where we experimentally derive a~3D rendering method.